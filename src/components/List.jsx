import React from 'react';

var placeholder = document.createElement("li");
placeholder.className = 'placeholder';

class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = { color: props.color };
        this.dragStart = this.dragStart.bind(this);
        this.dragEnd = this.dragEnd.bind(this);
        this.dragOver = this.dragOver.bind(this);
    }

    dragStart(e) {
        this.dragged = e.currentTarget;
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('text/plain', this.dragged.dataset.id);
    }

    dragEnd(e) {
        this.dragged.style.display = "block";
        this.dragged.parentNode.removeChild(placeholder);

        const data = [...this.state.color];
        const from = Number(this.dragged.dataset.id);
        let to = Number(this.over.dataset.id);
        if (from < to) to--;
        data.splice(to, 0, data.splice(from, 1)[0]);
        this.setState({ color: data });
    }

    dragOver(e) {
        e.preventDefault();
        this.dragged.style.display = 'none';
        const target = e.target.closest('li');
        if (!target) return;
        const bounding = target.getBoundingClientRect();
        const offset = bounding.y + bounding.height / 2;
        if (e.clientY - offset > 0) {
            target.parentNode.insertBefore(placeholder, target.nextSibling);
        } else {
            target.parentNode.insertBefore(placeholder, target);
        }
        this.over = target;
    }

    render() {
        const listItem = this.state.color.map((item, i) => (
            <li
                style={{ backgroundColor: item }}
                data-id={i}
                key={i}
                draggable='true'
                onDragStart={this.dragStart}
                onDragEnd={this.dragEnd}
            >
                {item}
            </li>
        ));

        return (
            <div>
                <ul onDragOver={this.dragOver}>
                    {listItem}
                </ul>
            </div>
        );
    }
}

export default List;
