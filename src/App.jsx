import React ,{ useState } from 'react'
import './App.css'
import List from './components/List'


class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      color :["red","blue","green","yellow","gray","black","orange"]
    }
  }
  
  render(){
    return(
      <div>
        <List color={this.state.color}/>
      </div>
    )
  }

}

export default App
